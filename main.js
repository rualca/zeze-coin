const { Blockchain } = require('./classes/blockchain.class');
const { Block } = require('./classes/block.class');

const zezeCoin = new Blockchain(4);
const block1 = new Block(1, '12/08/2018', { amount: 50 });
const block2 = new Block(2, '13/08/2018', { amount: 67 });

console.log(`Mining block 1...`);
zezeCoin.addBlock(block1);
console.log(`Mining block 2...`);
zezeCoin.addBlock(block2);

console.log(`
  Is this blockchain valid? ${zezeCoin.isChainValid()}
`);
console.log(JSON.stringify(zezeCoin));
