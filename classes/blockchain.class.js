const { Block } = require('./block.class');

exports.Blockchain = class Blockchain {
  constructor(difficulty) {
    this.chain = [this.createGenesisBlock()];
    this.difficulty = difficulty;
  }

  createGenesisBlock() {
    const block = new Block(0, '01/01/2018', 'this is genesis block', '0');
    return block;
  }

  getLatestBlock() {
    const lastBlockIndex = this.chain.length - 1;
    return this.chain[lastBlockIndex];
  }

  addBlock(newBlock){
    newBlock.previousHash = this.getLatestBlock().hash;
    newBlock.mineBlock(this.difficulty); 
    this.chain.push(newBlock);
  }

  isChainValid() {
    for (let i = 1; i < this.chain.length; i++) {
      const currentBlock = this.chain[i];
      const previousBlock = this.chain[i - 1];

      if (currentBlock.hash !== currentBlock.calculateHash()) {
        return false;
      }
      if (currentBlock.previousHash !== previousBlock.hash) {
        return false;
      } 
    }

    return true;
  }
}