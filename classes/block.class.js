const sha256 = require('crypto-js/sha256');

exports.Block = class Block {
  constructor(index, timestamp, data, previousHash = '') {
    this.index = index;
    this.timestamp = timestamp;
    this.data = data;
    this.previousHash = previousHash;
    this.hash = this.calculateHash();
    this.nonce = 0;
  }

  calculateHash() {
    const hash = sha256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data) + this.nonce).toString();
    return hash;
  }

  mineBlock(difficulty) {
    const zerosArray = Array(difficulty + 1).join('0');
    console.log(this.hash);
    let hashSubstring = this.hash.substring(0, difficulty);

    while (hashSubstring !== zerosArray) {
      this.nonce++;
      this.hash = this.calculateHash();
      hashSubstring = this.hash.substring(0, difficulty);
    }

    console.log(`Block mined: ${this.hash}`);
  }
}